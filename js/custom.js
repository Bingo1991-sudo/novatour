$(function(){

  var $menu = $(".js-menu");
  if(!$menu.length) return;

  var $menuBtn = $menu.find(".js-menu-item");

  $menuBtn.click(function(){
    if ($(this).hasClass('active')) {
      $menuBtn.removeClass('active');
    }
    else {
      $menuBtn.removeClass('active');
      $(this).addClass('active');
    }
  });

	$('.js-advantages-slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 481,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
    $('.js-brands-slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        },
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        },
        breakpoint: 481,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
    });
    $('.js-clients-slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        },
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        },
        breakpoint: 481,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        },
        breakpoint: 481,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
    });
});

$(function(){
var $formWr = $(".js-form-wr");
  if(!$formWr.length) return;

  var $form = $formWr.find(".js-form");
  var $formBtn = $formWr.find(".js-form-btn");
  var $overLay = $formWr.find(".js-overlay");

  $formBtn.click(function(){
    $(this).toggleClass('active');
    $form.toggleClass('active');
    $overLay.toggleClass('active');
  });
  $overLay.click(function(){
    $(this).toggleClass('active');
    $form.toggleClass('active');
    $formBtn.toggleClass('active');
  });
});
